var gulp = require('gulp');
var concat = require('gulp-concat');
var ts = require('gulp-typescript');
var browser = require('browser-sync');
var autoprefixer = require('gulp-autoprefixer');
var reload = browser.reload;

var tsProject = ts.createProject('tsconfig.json');

gulp.task('browsersync', function(){
	browser({
		server: {
			baseDir: "./dist/"
		}
	});
});

gulp.task("dojs", function(){
	var tsResult = gulp.src("src/script/**/*.ts")
		.pipe(ts(tsProject));
	
	tsResult.js.pipe(gulp.dest("dist/"));

	gulp.src('src/script.js', {base:"src/"})
		.pipe(gulp.dest('dist/'));
});
gulp.task("dohtml", function(){
	gulp.src('src/index.html', {base:"src/"})
		.pipe(gulp.dest('dist/'));
});
gulp.task("docss", function(){
	gulp.src('src/style.css', {base:"src/"})
		.pipe(autoprefixer({cascade:false}))
		.pipe(gulp.dest('dist/'));
});

gulp.task('reload', function(){
	reload();
});

gulp.task('buildall', function(){

});

gulp.task('watch', function(){
	gulp.watch(["src/**/*.ts"], ['dojs']);
	gulp.watch(["src/*.html"], ['dohtml']);
	gulp.watch(["src/*.css"], ['docss']);
	gulp.watch(["src/**/*.ts", "src/*.css", "src/*.html"], ['reload']);
});

gulp.task('default', ['dohtml','docss', 'dojs','browsersync', 'watch']);