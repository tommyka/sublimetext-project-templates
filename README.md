# README #

These templates are made for [STProjectMaker](https://github.com/bit101/STProjectMaker).
Install STProjectMaker through PackageManager.

Rememeber to make referance to you template folder and standard project folder in the STProjectMaker settings

    "template_path": "D:\\Code\\Tools\\sublime_templates",
    "default_project_path": "D:\\Code\\Javascript"

### What is this repository for? ###

* Project templates to get started quickly. Javascript and Typescript template is set up with gulpscripts for fast prototyping with browsersync and concating of files

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions