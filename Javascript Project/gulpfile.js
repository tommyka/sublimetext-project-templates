var gulp = require('gulp');
var concat = require('gulp-concat');
var browser = require('browser-sync');
var autoprefixer = require('gulp-autoprefixer');
var reload = browser.reload;

var outputName = "${project_name}.js";

gulp.task('browsersync', function(){
	browser({
		server: {
			baseDir: "./dist/"
		}
	});
});

gulp.task("dojs", function(){
	gulp.src("src/script/**/*.js")
		.pipe(concat(outputName))
		.pipe(gulp.dest("dist/"));

	gulp.src('src/script.js', {base:"src/"})
		.pipe(gulp.dest('dist/'));
});
gulp.task("dohtml", function(){
	gulp.src('src/index.html', {base:"src/"})
		.pipe(gulp.dest('dist/'));
});
gulp.task("docss", function(){
	gulp.src('src/style.css', {base:"src/"})
		.pipe(autoprefixer({cascade:false}))
		.pipe(gulp.dest('dist/'));
});

gulp.task('reload', function(){
	reload();
});

gulp.task('buildall', function(){

});

gulp.task('watch', function(){
	gulp.watch(["src/**/*.js"], ['dojs']);
	gulp.watch(["src/*.html"], ['dohtml']);
	gulp.watch(["src/*.css"], ['docss']);
	gulp.watch(["src/**/*.js", "src/*.css", "src/*.html"], ['reload']);
});

gulp.task('default', ['dohtml','docss', 'dojs','browsersync', 'watch']);